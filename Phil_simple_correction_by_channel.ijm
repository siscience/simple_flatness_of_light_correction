function array_median(array) {
    Array.sort(array);
    if (array.length % 2 == 0) {
        median = (array[array.length / 2] + array[array.length / 2 - 1]) / 2;
    } else {
        median = array[array.length / 2];
    }
    return median;
}


function roi_median() {
    Roi.getBounds(rx, ry, width, height);
    row = 0;
    pixel_index = 0;
    Roi.getContainedPoints(xpoints, ypoints);
    value_array = newArray(xpoints.length);
    for (y = ry; y < ry + height; y++) {
        for (x = rx; x < rx + width; x++) {
            if (Roi.contains(x, y) == 1) {
                value_array[pixel_index] = getPixel(x, y);
                pixel_index++;
            }
        }
    }
    return array_median(value_array);
}
image_title = getInfo("image.title")
image_title_WOext = replace( image_title , ".png" , "" ); ///if time, generalise
run("Select None");
run("Duplicate...", " ");
run("Split Channels");
red_channel = image_title_WOext + "-1.png (red)"
green_channel = image_title_WOext + "-1.png (green)"
blue_channel = image_title_WOext + "-1.png (blue)"

selectWindow(red_channel);
roiManager("Select", 0);
red_median = roi_median();
run("Select None");
//make correction matrix
run("32-bit");
division_string = "value="+ red_median;
run("Divide...", division_string);
run("Reciprocal");

selectWindow(green_channel);
roiManager("Select", 0);
green_median = roi_median();
run("Select None");
//make correction matrix
run("32-bit");
division_string = "value="+ green_median;
run("Divide...", division_string);
run("Reciprocal");

selectWindow(blue_channel);
roiManager("Select", 0);
blue_median = roi_median();
run("Select None");
//make correction matrix
run("32-bit");
division_string = "value="+ blue_median;
run("Divide...", division_string);
run("Reciprocal");

selectWindow(image_title);
getDimensions(width, height, channels, slices, frames);

red_mask_vals_array = newArray(width*height);
green_mask_vals_array = newArray(width*height);
blue_mask_vals_array = newArray(width*height);
selectWindow(red_channel);
i = 0;
for (x = 0; x < width; x++) {
	for (y = 0; y < height; y++) {
		red_mask_vals_array[i] = getPixel(x, y);
		i = i+1;
	}
}
close();

selectWindow(green_channel);
i = 0;
for (x = 0; x < width; x++) {
	for (y = 0; y < height; y++) {
		green_mask_vals_array[i] = getPixel(x, y);
		i = i+1;
	}
}
close();

selectWindow(blue_channel);
i = 0;
for (x = 0; x < width; x++) {
	for (y = 0; y < height; y++) {
		blue_mask_vals_array[i] = getPixel(x, y);
		i = i+1;
	}
}
close();


selectWindow("blank");
image_to_correct_title = getInfo("image.title");
run("Split Channels");
red_channel_to_correct = image_to_correct_title + " (red)";
green_channel_to_correct = image_to_correct_title + " (green)";
blue_channel_to_correct = image_to_correct_title + " (blue)";
selectWindow(red_channel_to_correct);
run("32-bit");
i = 0;
for (x = 0; x < width; x++) {
	for (y = 0; y < height; y++) {
		px_val_img = getPixel(x, y);
		new_px_val_img = px_val_img*red_mask_vals_array[i];
		setPixel(x, y, new_px_val_img);
		i = i+1;
	}
}
selectWindow(green_channel_to_correct);
run("32-bit");
i = 0;
for (x = 0; x < width; x++) {
	for (y = 0; y < height; y++) {
		px_val_img = getPixel(x, y);
		new_px_val_img = px_val_img*green_mask_vals_array[i];
		setPixel(x, y, new_px_val_img);
		i = i+1;
	}
}
selectWindow(blue_channel_to_correct);
run("32-bit");
i = 0;
for (x = 0; x < width; x++) {
	for (y = 0; y < height; y++) {
		px_val_img = getPixel(x, y);
		new_px_val_img = px_val_img*blue_mask_vals_array[i];
		setPixel(x, y, new_px_val_img);
		i = i+1;
	}
}
merge_string = "c1=[" + red_channel_to_correct +"] c2=[" + green_channel_to_correct +"] c3=[" + blue_channel_to_correct +"] create ignore";
run("Merge Channels...", merge_string);
run("RGB Color");
rename(image_to_correct_title);
close('Composite');